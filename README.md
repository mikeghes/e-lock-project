The E-Lock team is creating the revolutionary bike lock. Our product has several functions very useful to leave your bike, your scooter or other engines without fear of theft.
We have an alarm to scare the thief who touches or move your bike. This alarm is connected with the IFTTT app on your phone so you receive a notification when the alarm sounds. This used a buzzer and a gyroscope.
 To unlock the product, you have two ways to do it. The first one is the manual with a potentiometer that you turn, the current number is displayed on the screen and if you want to validate your number, push the red button. After the right four-digit, the e-lock is open. If it’s wrong the alarm rings.
The second way is the phone, you push the button in your phone to unlock it.
You can change the password from your phone with the « particle » app. The password is stored in the EEPROM.

![image.png](./image.png)
