/*
 * Project e-Lock
 * Description:
 * Author: 
 *      BEYOUDH Zeineb
 *      GRONDIN Mathieu
 *      GERNATH Adrien
 *      GHESQUIERE Michaël
 *      GUIHARD Charly
 * Date:  04/01/2022
 */
#include "SparkFunMicroOLED.h"
#include "SparkFunMMA8452Q.h"
#include <stdlib.h>
//#include "Timer.h"

/*********** DECLARATIONS ***********/
#define LOCKED      0
#define UNLOCKED    1
#define ALARM       2
#define NO_ALARM    3
#define LOCK_BIKE   4
#define UNLOCK_BIKE 5

MicroOLED oled;
MMA8452Q accel; // Default constructor, SA0 pin is HIGH
//Timer t;

/*********** PIN DEFINITION ***********/
int button        = D4;
int piezo         = D3;
int servo         = D2; //to open
int potentiometer = A0;


/******** VARIABLE DEFINITION ********/
int potValue      = 0;
Servo myservo;  // create servo object to control a servo
int valueServo    = 0;
volatile bool iState = false;
bool iStateFlag = false;
float AccX ;
float AccY ;
float AccZ ;
char passwordTrue[] = {'1','1','1','1'};
char password[] = {'0', '0', '0', '0'};
int counterPassword = 0;
int timer_screen = 0;
String res = "0";
String last_res;
int bikeStateValue = -1;
int valeur_eeprom;
const int address=221;

/********* FUNCTIONS AVAILABLE ***********/
// Reading of the potentiometer - TOTEST
String potReading();

// Update the LED state - TOTEST
void bikeState(int state);

//  Play sound regarding the situation - TODO
void playSound(int event);

// Print the number entered on the OLED - TODO
void printNumberOLED(String number, int font);

// Play an animation on the OLED - TODO
void playAnimationOLED(int event);

// Move the SERVO - TOTEST
void moveServo(int event);

// Read the state - TODO
void buttonState();

// Check if 
int checkACC();

// Unlock bike with IFTTT button
int buttonUnlockBike(String data);

// Lock bike with IFTTT button
int buttonLockBike(String data);

//Change the password with Particle app
int changePasswd(String data);

// Load password
void loadPasswd(int number4);


//**************************************************************** Variables for the Buzzer 
/*  RickRollCode

    AUTHOR: Rowan Packard
    rowanpackard@gmail.com

    DISCLAIMER: The song "Never Gonna Give You Up" by Rick Astley
    is not the creative property of the author. This code simply
    plays a Piezo buzzer rendition of the song.

    Adaptation by Michaël Ghesquière
*/

#define  a3f    208     // 208 Hz
#define  b3f    233     // 233 Hz
#define  b3     247     // 247 Hz
#define  c4     261     // 261 Hz MIDDLE C
#define  c4s    277     // 277 Hz
#define  e4f    311     // 311 Hz    
#define  f4     349     // 349 Hz 
#define  a4f    415     // 415 Hz  
#define  b4f    466     // 466 Hz 
#define  b4     493     // 493 Hz 
#define  c5     523     // 523 Hz 
#define  c5s    554     // 554 Hz
#define  e5f    622     // 622 Hz  
#define  f5     698     // 698 Hz 
#define  f5s    740     // 740 Hz
#define  a5f    831     // 831 Hz 

#define rest    -1


volatile int beatlength = 100; // determines tempo
float beatseparationconstant = 0.3;

int threshold;

int a; // part index
int b; // song index
int c; // lyric index

boolean flag;


int song1_chorus_melody[] =
{ b4f, b4f, a4f, a4f,
  f5, f5, e5f, b4f, b4f, a4f, a4f, e5f, e5f, c5s, c5, b4f,
  c5s, c5s, c5s, c5s,
  c5s, e5f, c5, b4f, a4f, a4f, a4f, e5f, c5s,
  b4f, b4f, a4f, a4f,
  f5, f5, e5f, b4f, b4f, a4f, a4f, a5f, c5, c5s, c5, b4f,
  c5s, c5s, c5s, c5s,
  c5s, e5f, c5, b4f, a4f, rest, a4f, e5f, c5s, rest
};

int song1_chorus_rhythmn[] =
{ 1, 1, 1, 1,
  3, 3, 6, 1, 1, 1, 1, 3, 3, 3, 1, 2,
  1, 1, 1, 1,
  3, 3, 3, 1, 2, 2, 2, 4, 8,
  1, 1, 1, 1,
  3, 3, 6, 1, 1, 1, 1, 3, 3, 3, 1, 2,
  1, 1, 1, 1,
  3, 3, 3, 1, 2, 2, 2, 4, 8, 4
};


//***************************************** SETUP
void setup() {

  // Activate card led RGB control
  RGB.control(true);

  pinMode(piezo,OUTPUT);
  pinMode(servo,OUTPUT);
  

  oled.begin();    // Initialize the OLED
  oled.clear(ALL); // Clear the display's internal memory
  oled.display();  // Display what's in the buffer (splashscreen)

  pinMode(button, INPUT_PULLUP);
  attachInterrupt(button, buttonState, RISING);

  Serial.begin(9600); // Serial is used to print sensor readings.

  pinMode(potentiometer, INPUT); // potentiometers are an analog input

  myservo.attach(D2);  // attaches the servo on pin to the servo object

  accel.begin(SCALE_2G, ODR_1); // Set up accel with +/-2g range, and slowest (1Hz) ODR

  //Definition of the initial position
  accel.read();
  AccX = accel.cx;
  AccY = accel.cy;
  AccZ = accel.cz;
  //t.every(8000,checkACC);

  //Setup for buzzer
  pinMode(piezo, OUTPUT);
  Serial.begin(9600);
  flag = true;
  a = 4;
  b = 0;
  c = 0;

  Particle.function("buttonUnlockBike", buttonUnlockBike);
  Particle.function("buttonLockBike", buttonLockBike);
  Particle.function("changePasswd",changePasswd);

  EEPROM.get(address,valeur_eeprom);
  loadPasswd(valeur_eeprom);
}


//***************************************** MAIN LOOP
void loop() {
  if(checkACC())
    playSound(ALARM);

  //Reading of the value of the pot.
  last_res = res;
  res = potReading();

  //Print le value on the OLED
  //printNumberOLED(String(res), 2);
  delay(15);
  

  //If the button is pressed
  if(iState)
  {
    iState = false;
    //Debug function :)
    //Serial.println("Code saisi : " + res);

    //Print the value on the OLED
    printNumberOLED(String(res), 1);

    //-----Treatment of the password
    
    //Add the number to the password
    password[counterPassword] = res[0];

    //If we have a good password (equal)
    if(passwordTrue[0] == password[0] && passwordTrue[1] == password[1] && passwordTrue[2] == password[2] && passwordTrue[3] == password[3])
    {
      //Serial.println("Locker is opening");
      moveServo(UNLOCK_BIKE);
      playSound(NO_ALARM);
    }

    //Debug printing
    Serial.println("PasswordOK : " + String(passwordTrue[0]) + String(passwordTrue[1]) + String(passwordTrue[2]) + String(passwordTrue[3]));
    Serial.println("Password : " + String(password[0]) + String(password[1]) + String(password[2]) + String(password[3]));
    
    counterPassword++;

    //Debug printing
    Serial.println("counter : " + String(counterPassword) + "\n");

    //Il we have 4 numbers we reset
    if(counterPassword == 4)
    {
      counterPassword = 0;
      password[0] = '0';
      password[1] = '0';
      password[2] = '0';
      password[3] = '0';

      //Serial.println("Reset password !");  
    }

  }

  
  if(timer_screen>10){
    oled.clear(PAGE);
    oled.display();
    password[0] = '0';
    password[1] = '0';
    password[2] = '0';
    password[3] = '0';
  }else{
    //Print le value on the OLED
    printNumberOLED(String(res), 2);
    delay(15);
  }
  if(iStateFlag || (last_res != res)){
    timer_screen=0;
  }
  timer_screen++;
  iStateFlag = false;
  //Serial.println(iStateFlag);
}


// Update the LED state
void bikeState(int state){
  
  if(state == LOCKED)
  {
    // BLUE led is on, the rest is off
    RGB.color(0, 0, 255);
  }
  else if (state == UNLOCKED)
  {
    // GREEN led is on, the rest is off
    RGB.color(0, 255, 0);
  }
  else if (state == ALARM)
  {
    // RED led is on, the rest is off
    RGB.color(255, 0, 0);
  }
}


// Reading of the potentiometer
String potReading(){
  potValue = (analogRead(potentiometer)/16)/28; //In order to have digit code from 0 to 9

  return String(potValue);
}

// Play sound regarding the situation
void playSound(int event){
  if (event == ALARM)
  {
    tone(piezo,440,200);
    delay(1000);
    tone(piezo,440,200);
    delay(1000);
    tone(piezo,440,200);
    Particle.publish("alarm_running");
    bikeState(ALARM);
  }
  else if (event == NO_ALARM)
  {
    for (int i = 0; i < 59; i++)
    {
      //Serial.println("i :"+String(i));
      int notelength;
      notelength = beatlength * song1_chorus_rhythmn[b];
      if (song1_chorus_melody[b] > 0) {
        tone(piezo, song1_chorus_melody[b], notelength);
        c++;
      }
      b++;
      if (b >= sizeof(song1_chorus_melody) / sizeof(int)) {
        a++;
        b = 0;
        c = 0;
      }
      delay(notelength);
      noTone(piezo);
      delay(notelength * beatseparationconstant);
      if (a == 7) { // loop back around to beginning of song
        a = 1;
      }
    }    
  }
}

// Print the number entered on the OLED
void printNumberOLED(String title, int font)
{
  int middleX = oled.getLCDWidth() / 2;
  int middleY = oled.getLCDHeight() / 2;

  oled.clear(PAGE);
  oled.setFontType(font);
  // Try to set the cursor in the middle of the screen
  oled.setCursor(middleX - (oled.getFontWidth() * (title.length()/2)),
                 middleY - (oled.getFontWidth() / 2));
  // Print the title:
  oled.print(title);
  oled.display();
  delay(1500);
  oled.clear(PAGE);
  
}

// Play an animation on the OLED
void playAnimationOLED(int event)
{
  if(event == ALARM)
  {
    //Play alarm animation
    //TODO : code the alarm animation
  }
  else if (event == NO_ALARM)
  {
    //Play openning animation
    //TODO : code the animation
  }
}

// Move the SERVO
void moveServo(int event)
{
  //Erase OLED
  oled.clear(ALL);
  oled.display();

  if(event == UNLOCK_BIKE)
  {
    valueServo = 100;
    //Serial.println("Bike unlocked \n");
    bikeState(UNLOCKED);
  }
  else if (event == LOCK_BIKE)
  {
    valueServo = 0;
    //Serial.println("Bike locked \n");
    bikeState(LOCKED);    
  }

  //Move the servo to close/open the locker
  myservo.write(valueServo);             
  delay(500);
}

// Read the button state
void buttonState()
{
  iState = true;
  iStateFlag = true;
  //Serial.println("Interrupt");
}

// Read 
int checkACC()
{
  float seuil = 0.1;

  // accel.available() will return 1 if new data is available, 0 otherwise
  if (accel.available())
  {
    accel.read();
    // 
    
    if( (abs(AccX - accel.cx) > seuil) || (abs(AccY - accel.cy) > seuil) || (abs(AccZ - accel.cz) > seuil))
    {
      Serial.println("Bike moved !\n");
      playSound(ALARM);

      //Definition of the initial position
      accel.read();
      AccX = accel.cx;
      AccY = accel.cy;
      AccZ = accel.cz;
      
      return -1;
    }
    
  }

  return 0;
}

// Unlock bike with IFTTT button
int buttonUnlockBike(String data)
{
  //Serial.println("Locker is opening");
  moveServo(UNLOCK_BIKE);
  playSound(NO_ALARM);

  counterPassword = 0;
  password[0] = '0';
  password[1] = '0';
  password[2] = '0';
  password[3] = '0';

  //Serial.println("Reset password !");

  return 0;
}

// Unlock bike with IFTTT button
int buttonLockBike(String data)
{
 
  moveServo(LOCK_BIKE);

  counterPassword = 0;
  password[0] = '0';
  password[1] = '0';
  password[2] = '0';
  password[3] = '0';


  return 0;
}

//Change the password with Particle app
int changePasswd(String data){
  passwordTrue[0] = data[0];
  passwordTrue[1] = data[1];
  passwordTrue[2] = data[2];
  passwordTrue[3] = data[3];
  EEPROM.put(address,atoi(data));
  return 0;
}


// Load password
void loadPasswd(int number4)
{
  int Nb_zero;
  int Nb_two;
  int Nb_tree;
  int Nb_four;

  Nb_zero = number4 / 1000;
  Nb_two = (number4 - (Nb_zero*1000) ) / 100;
  Nb_tree = (number4 - (Nb_zero*1000) - (Nb_two*100) ) / 10;
  Nb_four = (number4 - (Nb_zero*1000) - (Nb_two*100) - (Nb_tree*10));


  String data = String(Nb_zero) + String(Nb_two) + String(Nb_tree) + String(Nb_four);
  Serial.println(data);

  passwordTrue[0] = data[0];
  passwordTrue[1] = data[1];
  passwordTrue[2] = data[2];
  passwordTrue[3] = data[3];

}